const express = require("express");
const router = express.Router();
const { transaction,product,member} = require("../models");

//Transaction
router.get("/transaction", async (req,res)=>{
  const transactions = await transaction.findAll({
      include: [product,member]
  })
      // res.send (transactions)
      res.render("transactions", {transactions:transactions})
})

router.get("/add-transaction", async (req, res)=>{
  const transactions = await transaction.findAll({
      include: [product,member]
  })
  res.render("addTransaction", {transactions:transactions})

});

router.post("/transaction", async (req, res) => {
  if (req.body._method) {
      await transaction.update(req.body, {
         where: {
          id: req.body.id,
         } 
      })
  } else {
      await transaction.create(req.body)
  }
  res.redirect('/transaction')
})

//EDIT TRANSACTION
router.get("/edit-transaction/:id", async (req, res)=>{
  const transactions = await transaction.findOne({
      where: {
          id: req.params.id
      }
  })
  res.render("editTransaction", { transactions: transactions })
});

//DELETE TRANSACTION
router.post('/delete-transaction', async (req, res) => {
  if (req.body._method === "DELETE") {
      await transaction.destroy({
        where: {
          id: req.body.id,
        },
      });
    }
  res.redirect('/transaction');
});

module.exports = router;
