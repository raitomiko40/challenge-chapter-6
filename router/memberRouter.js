const express = require("express");
const router = express.Router();
const { member } = require("../models");

router.get("/member", async (req, res) => {
  const members = await member.findAll({

  });
  res.render("member", { members: members });
});

router.get("/add-member", async (req, res)=>{
  const members = await member.findAll({
     
  })
  res.render("addMember", {members:members})

});

router.post("/member", async (req, res) => {
  if (req.body._method) {
      await member.update(req.body, {
         where: {
          id: req.body.id,
         } 
      })
  } else {
      await member.create(req.body)
  }
  res.redirect('/member')
})


// EDIT MEMBER
router.get("/edit-member/:id", async (req, res)=>{
  const members = await member.findOne({
      where: {
          id: req.params.id
      }
  })
  res.render("editMember", { members: members})
});

// DELETE MEMBER
router.post('/delete-member', async (req, res) => {
  if (req.body._method === "DELETE") {
      await member.destroy({
        where: {
          id: req.body.id,
        },
      });
    }
  res.redirect('/member');
});

module.exports = router;
