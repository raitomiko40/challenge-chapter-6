const express = require("express");
const router = express.Router();
const { product } = require("../models");

router.get("/product", async (req, res) => {
  const products = await product.findAll({
 
  });
  res.render("products", { products:products});
});

router.get("/add-product", async (req, res)=>{
  const products = await product.findAll({
   
  })
  res.render("addProduct", {products:products})

});

router.post("/product", async (req, res) => {
  if (req.body._method) {
      await product.update(req.body, {
         where: {
          id: req.body.id,
         } 
      })
  } else {
      await product.create(req.body)
  }
  res.redirect('/product')
})


// EDIT Product
router.get("/edit-product/:id", async (req, res)=>{
  const products = await product.findOne({
      where: {
          id: req.params.id
      }
  })
  res.render("editProduct", { products:products})
});

// DELETE Product
router.post('/delete-product', async (req, res) => {
  if (req.body._method === "DELETE") {
      await product.destroy({
        where: {
          id: req.body.id,
        },
      });
    }
  res.redirect('/product');
});

module.exports = router;
