const express = require ("express")
const app = express()
const port = 3000
const memberData = require("./router/memberRouter")
const productData = require("./router/productRouter")
const transactionData = require("./router/transactionRouter")
const loginController = require("./controllers/loginController")

const {member, product, transaction} = require("./models")
// const transactions = require("./models/transaction")

app.use(express.json()) 
app.use(express.urlencoded()) 

app.use(express.static('public'))
app.set('view engine', 'ejs')

app.use("/", memberData)
app.use("/", productData)
app.use("/", transactionData)
app.use(loginController)

app.get("/", async(req,res)=>{
    res.redirect('/login')
})



// dashborad
app.get('/dashboard', (req, res) => {
    res.render("dashboard");
  });


app.listen(port, () =>{
    console.log("server running on port  " + port);
})